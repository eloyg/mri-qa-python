# MRI QA Python

# Authors
This code was first implemented in Matlab by Tonya Jo Hanson White and described in the following paper:
 https://www.researchgate.net/publication/321578633_Automated_quality_assessment_of_structural_magnetic_resonance_images_in_children_Comparison_with_visual_inspection_and_surface-based_reconstruction

The code has been implemented in Python by: Pierre-Olivier Quirion, Sofiya Bogdan and Eloy Geenjaar

# Prerequisite Python modules
 	os 
 	sys 
 	argparse
 	numpy
 	nibabel
 	shutil 
 	nipype

# How to use
 The code can be tested on the provided test image: 'MNI152_T1_1mm.nii.gz' as follows:
 
	python tw-qa.py -i MNI152_T1_1mm.nii.gz
 	python3 tw-qa.py -i MNI152_T1_1mm.nii.gz

The inputs to the function are specified as follows:

MRI QA

optional arguments:

	-h, --help	show this help message and exit

	--inputs INPUTS, -i INPUTS 	A g-zipped Nifti file or a directory including
                        g-zipped Nifti files
  	--output OUTPUT, -o OUTPUT	The path where the results are stored
	--save SAVE, -s SAVE 	Save which scans have been completed
	--savefile SAVEFILE, -sf SAVEFILE	The file in which the names of the completed scans are
                        saved
	 --clean CLEAN, -c CLEAN	Clean up the temporary folder
	--verbose VERBOSE, -v VERBOSE	Turn on verbosity for the transformations
	 --topdown TOPDOWN, -td TOPDOWN	The number of voxels inferior to the top of the head
                        to stop the search for the edge
	--topstart TOPSTART, -ts TOPSTART	The number of voxels inferior to the top of the head
                        to start the search for the edge
	--eye EYE, -e EYE  		The number of voxels from the anterior side of the
                        head to start the search for the edge. This is due to
                        potential eye artefacts
	--imgthresh IMGTHRESH, -it IMGTHRESH	The threshold for the edge in the original image
	--edgethresh EDGETHRESH, -et EDGETHRESH	The threshold for the edge in the generated edge file
	--countback COUNTBACK, -cb COUNTBACK	The lateral distance in voxels between the edge and the voxel used to calculate the gradient
	--badscan BADSCAN, -bs BADSCAN	The maximum number of voxels to check when the edge is
                        not found for bad scans

