##############################################################################################

# An automatic quality assessment tool that calculates the gradient of structural (T1) images

# Author:           Tonya Jo Hanson White & Pierre-Olivier Quirion
# With help from:   Sofiya Bogdan & Eloy Geenjaar
# Date:             03/02/2019                      (dd/mm/yyyy)

##############################################################################################

__version__ = 1.0

import os
import sys
import argparse
import numpy as np
import nibabel as nb
from shutil import copyfile
import nipype.interfaces.fsl as fsl
import nipype.interfaces.afni as afni

class calculate_gradient(object):

    def safely_create_dir(self,directory):
        if not os.path.isdir(directory):
            try:
                os.makedirs(directory)
            except OSError as e:
                if e.errno != e.errno.EEXIST:
                    raise Exception("Directory: "+str(directory)+" could not be created")

    def safely_create_file(self,path):
        if not os.path.isfile(path):
            try:
                open(path,'w+').close()
            except OSError as e:
                raise Exception("File: "+str(path)+" could not be created")

    def write_to_file(self,path,string_to_write):
        if os.path.isfile(path):
            try:
                # Back up original file just in case
                if os.path.isfile(path+'.bak'):
                    copyfile(path,path+'.bak')
                    f = open(path,'a+')
                    f.write(string_to_write)
                    f.close()
                else:
                    self.safely_create_file(path+'.bak')
                    copyfile(path,path+'.bak')
                    f = open(path,'a+')
                    f.write(string_to_write)
                    f.close()
            except OSError as e:
                raise Exception("File: "+str(path)+" could not be found")
        else:
            print(path+" can not be written to because it does not exist")


    def __init__(self, input_qa, results=None, save_completed=True, completed_file=None, 
                clean_up=True, verbosity=False, topdown=80, topstart=10, eye=50, 
                img_thresh=1000, edge_thresh=500, count_back=5, 
                badscan_maxcount_back=50):
        """
        input_qa = input image or folder with input images
        results = the file that the results should be stored in
        save_completed = whether or not the scans that have been completed should be stored
        completed_scans = the file that the scans that have been completed are stored in, if this is the first time running the algorithm this file will be made
        clean_up = whether or not the files that are created while running the algorithm should be deleted after the run
        verbosity = whetherthe algorithm should be verbose

        The following parameters were obtained by the author manually and may not apply to every scanner / image
        topdown = The number of voxels inferior to the top of the head to stop the search for the edge
        topstart = The number of voxels inferior to the top of the head to start the search for the edge
        eye = The number of voxels from the anterior side of the head to start the search for the edge. This is due to potential eye artefacts
        img_thresh = The threshold for the edge in the original image
        edge_thresh = The threshold for the edge in the generated edge file
        count_back = The lateral distance in voxels between the edge and the voxel used to calculate the gradient
        badscan_maxcount_back = The maximum number of voxels to check when the edge is not found for bad scans
        """
        
        # The directory all the temporary files are stored in, the directory and
        # its contents are generally removed after running the algorithm
        # if clean_up is set to False, the directory will not be removed
        tmp_dir = './temp/'

        self.safely_create_dir(tmp_dir)

        self.tmp_dir = tmp_dir
        self.dir = False

        if os.path.isdir(input_qa):
            self.dir = True
            self.input_dir = input_qa
        else:
            if not os.path.isfile(input_qa):
                raise Exception('Input file/directory does not exist')
            else:
                self.dir = False
                self.input_qa = input_qa
        # If the results file/folder is not specified, a results folder will be made with a results file in it
        if results is None:
            self.safely_create_dir('./results/')
            self.safely_create_file('./results/results.txt')
            self.results = './results/results.txt'
        else:
            if not os.path.isfile(results):
                raise Exception("Results file: "+results+" does not exist")
            else:
                self.results = results
        self.save_completed = save_completed
        if self.save_completed:
            # Check whether the completed file is specified
            if completed_file is None:
                if not os.path.isfile('./completed_scans/completed_scans.txt'):
                    self.safely_create_dir('./completed_scans/')
                    self.safely_create_file('./completed_scans/completed_scans.txt')
                    self.completed_file = './completed_scans/completed_scans.txt'
                    self.completed_scans = []
                else:
                    with open('./completed_scans/completed_scans.txt') as f:
                        completed_scans = f.readlines()
                    self.completed_scans = [x.strip() for x in completed_scans]
                    self.completed_file = './completed_scans/completed_scans.txt'
            else:
                if os.path.isfile(completed_file):
                    with open(completed_file) as f:
                        completed_scans = f.readlines()
                    self.completed_scans = [x.strip() for x in completed_scans]
                    self.completed_file = completed_file
                else:
                    raise Exception("The provided completed file does not exist")


        self.clean_up = clean_up
        self.verbosity = verbosity
        self.topdown = topdown
        self.topstart = topstart
        self.eye = eye
        self.img_thresh = img_thresh
        self.edge_thresh = edge_thresh
        self.count_back = count_back
        self.badscan_maxcount_back = badscan_maxcount_back

        if not self.dir:
            if self.input_qa.endswith('.nii.gz'):
                path_without_ext, ext = self.input_qa.split(os.path.extsep, 1)
                scan_name = os.path.basename(path_without_ext)
                self.scan_name = scan_name
            else:
                raise Exception("The provided input file is not a gzipped NIFTI file")
            if self.scan_name not in self.completed_scans:
                self.generate_edges()
            else:
                pass
        else:
            for f in os.listdir(self.input_dir):
                self.input_qa = os.path.join(self.input_dir, f)
                if self.input_qa.endswith('nii.gz'):
                    path_without_ext, ext = self.input_qa.split(os.path.extsep, 1)
                    scan_name = os.path.basename(path_without_ext)
                    self.scan_name = scan_name
                    if self.scan_name not in self.completed_scans:
                        self.generate_edges()
                    else:
                        pass

        os.rmdir(self.tmp_dir)

    def generate_edges(self):
        
        # Standardised paths for the files that are created during the
        # transformation(s)
        self.reoriented_input = self.tmp_dir + 'temp_reoriented_' + self.scan_name + '.nii.gz'
        self.out_file_bet = self.tmp_dir + 'temp_bet_' + self.scan_name + '.nii.gz'
        self.bet_mask_path = self.tmp_dir + 'temp_bet_' + self.scan_name + '_mask.nii.gz'
        self.edge_path = self.tmp_dir + 'temp_3dedge_' + self.scan_name + '.nii.gz'
        self.bet_edge_path = self.tmp_dir + 'temp_bet_' + self.scan_name + '_outskin_mesh.nii.gz'

        fsl.FSLCommand.set_default_output_type('NIFTI_GZ')

        # Reorienting the image to match the approximate orientation of the standard template images (MNI152)
        reorient = fsl.Reorient2Std()
        reorient.inputs.in_file = self.input_qa
        reorient.inputs.out_file = self.reoriented_input
        results_reorient = reorient.run()

        # Generate the edges for ROI definition
        edge3 = afni.Edge3()
        edge3.inputs.in_file = self.reoriented_input
        edge3.inputs.out_file = self.edge_path
        edge3.inputs.verbose = self.verbosity
        result_edge3 = edge3.run()

        # Generate binary brain mask and the bet edges
        bet = fsl.BET()
        bet.inputs.in_file = self.reoriented_input
        bet.inputs.mask = True
        bet.inputs.surfaces = True
        bet.inputs.out_file = self.out_file_bet
        result_bet = bet.run()

        try:
            mean_gradient = self.gradient()

        # If there is a runtime or index error this generally means that the edge 
        # could not be found which implies that this image is either bad or 
        # that the wrong threshold is chosen
        except RuntimeError as e:
            mean_gradient = 0.0

        except IndexError as e:
            mean_gradient = 0.0

        # Write to results
        self.write_to_file(self.results,(self.scan_name+', '+str(mean_gradient)+'\n'))
        # Write to completed file
        if self.save_completed:
            self.write_to_file(self.completed_file,self.scan_name+'\n')

        # Remove all the temporary files
        if self.clean_up:
            self.clean()

    def clean(self):
        for file_name in os.listdir(self.tmp_dir):
            path = os.path.join(self.tmp_dir,file_name)
            try:
                if os.path.isfile(path):
                    os.unlink(path)
            except Exception as e:
                raise


    def gradient(self):
        
        # For each row in the region of interest, calculate the gradient
        # along the edge

        # Getting the data from the image, edge, BET mask and BET edge using Nibabel
        img = nb.load(self.reoriented_input).get_data()
        edge = nb.load(self.edge_path).get_data()
        mask = nb.load(self.bet_mask_path).get_data()
        bet_edge = nb.load(self.bet_edge_path).get_data()

        # Finding the top of the brain
        topbrain = img.shape[2]
        # Iterate from superior to inferior
        for z in reversed(range(img.shape[2])):
            topbrain = topbrain - 1
            # Iterate from anterior to posterior
            for y in reversed(range(img.shape[1])):
                # Iterate from dexter to sinister
                for x in range(img.shape[0]):
                    if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                        break
                if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                    break
            if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                break

        # 80 mm inferior from the top of the head
        z_start = topbrain - self.topdown
    
        # 10 mm inferior from the top of the head
        z_end = topbrain - self.topstart

        # Finding the posterior side of the brain
        posterior = 0

        # Iterate from posterior to anterior
        for y in range(img.shape[1]):
                posterior = posterior + 1
                # Iterate from inferior to superior
                for z in range(z_start,z_end):
                        # Iterate from dexter to sinister
                        for x in range(img.shape[0]):
                                if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                                        break
                        if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                                break
                if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                        break

        #Finding the anterior side of the brain
        anterior = img.shape[1]

        # Iterate from anterior to posterior
        for y in reversed(range(img.shape[1])):
                anterior = anterior - 1
                # Iterate from dexter to sinister
                for x in range(img.shape[0]):
                        # Iterate from inferior to superior
                        for z in range(z_start,z_end):
                                if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                                        break
                        if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                                break
                if img[x,y,z] > self.img_thresh and edge[x,y,z] > self.edge_thresh:
                        break

        # Subtract the size of the eyes from the anterior side of the brain
        y_end = anterior - self.eye
        y_start = posterior

        differences = []
        total_differences_right_left = []
        total_differences_left_right = []

        # Iterate from inferior to superior
        for z in range(z_start,z_end+1):
                # Iterate from anterior to posterior
                for y in reversed(range(y_start, y_end+1)):
                        # Iterate from dexter to sinister, stopping in the middle
                        for x in range(int(img.shape[0]/2)):
                                if bet_edge[x,y,z]==1:
                                        # For poor quality scans, bet creates an edge line beyond the actual edge, so we need to count the voxels until the correct edge  
                                        for count in range(self.badscan_maxcount_back): # 50 is a large enough random number
                                                if img[x+count,y,z]>self.img_thresh and mask[x+count,y,z]<1:
                                                        first_edge_encounter = x + count
                                                        # The gradient is calculated using a value 5 voxels from the edge
                                                        five_voxels_back = first_edge_encounter - self.count_back
                                                        # Difference:
                                                        diff = abs(img[first_edge_encounter,y,z] - img[five_voxels_back,y,z])
                                                        if diff:
                                                                # Append differences to list to create an array
                                                                differences.append(diff)
                                                        # Stops counting when the first edge (head edge) has been reached
                                                        break
                                else:
                                        continue
                                break

                        # If the list with differences exist, append it
                        if differences:
                            total_differences_right_left.append(differences)
                        
                        differences = []

                        # Iterate from sinister to dexter, stopping in the middle
                        for x in reversed(range(int(img.shape[0]/2),img.shape[0])):
                                if bet_edge[x,y,z]==1:
                                        # For poor quality scans, bet creates an edge line beyond the actual edge, so we need to count the voxels until the correct edge
                                        for count in range(self.badscan_maxcount_back): # 50 is a large enough random number
                                                if img[x-count,y,z]>self.img_thresh and mask[x-count,y,z]<1:
                                                        first_edge_encounter = x - count
                                                        five_voxels_back = first_edge_encounter + self.count_back
                                                        # Difference:
                                                        diff = abs(img[first_edge_encounter,y,z] - img[five_voxels_back,y,z])
                                                        if diff:
                                                                # Append differences to list to create an array
                                                                differences.append(diff)
                                                        # Stops counting when the first edge (head edge) has been reached
                                                        break
                                else:
                                        continue
                                break
                        if differences:
                                total_differences_left_right.append(differences)
                        differences = []
        
        # Convert lists to numpy arrays
        total_differences_right_left = np.asarray(total_differences_right_left)
        total_differences_left_right = np.asarray(total_differences_left_right)


        right_hemisphere = np.average(total_differences_right_left)
        left_hemisphere = np.average(total_differences_left_right)

        # The average of the left and right hemisphere averaged
        mean_gradient = np.average(np.average(np.vstack((left_hemisphere,right_hemisphere)),axis=0))


        return mean_gradient

def main(args=None):

    if args is None:
        args = sys.argv[1:]
    
    parser = argparse.ArgumentParser(description = 'MRI QA')
    parser.add_argument("--inputs", "-i", type=str, required=True,
                        help='A g-zipped Nifti file or a directory including g-zipped Nifti files')
    parser.add_argument("--output", "-o", type=str, default=None,
                        help='The path where the results are stored')
    parser.add_argument("--save", "-s", type=bool, default=True,
                        help="Save which scans have been completed")
    parser.add_argument("--savefile", "-sf", type=str, default=None,
                        help="The file in which the names of the completed scans are saved")
    parser.add_argument("--clean", "-c", type=int, default=True,
                        help='Clean up the temporary folder')
    parser.add_argument("--verbose", "-v", type=bool, default=False,
                        help='Turn on verbosity for the transformations')
    parser.add_argument("--topdown", "-td", type=int, default=80,
                        help='The number of voxels inferior to the top of the head to stop the search for the edge')
    parser.add_argument("--topstart", "-ts", type=int, default=10,
                        help='The number of voxels inferior to the top of the head to start the search for the edge')
    parser.add_argument("--eye", "-e", type=int, default=50,
                        help='The number of voxels from the anterior side of the head to start the search for the edge. This is due to potential eye artefacts')
    parser.add_argument("--imgthresh", "-it", type=int, default=1000,
                        help='The threshold for the edge in the original image')
    parser.add_argument("--edgethresh", "-et", type=int, default=500,
                        help='The threshold for the edge in the generated edge file')
    parser.add_argument("--countback", "-cb", type=int, default=5,
                        help='The lateral distance in voxels between the edge and the voxel used to calculate the gradient')
    parser.add_argument("--badscan", "-bs", type=int, default=50,
                        help='The maximum number of voxels to check when the edge is not found for bad scans')
    
    parsed = parser.parse_args(args)
    
    inputs = parsed.inputs
    output = parsed.output
    save = parsed.save
    save_file = parsed.savefile
    clean_up = parsed.clean
    verbosity = parsed.verbose
    topdown = parsed.topdown
    topstart = parsed.topstart
    eye = parsed.eye
    img_thresh = parsed.imgthresh
    edge_thresh = parsed.edgethresh
    count_back = parsed.countback
    badscan_maxcount_back = parsed.badscan

    calculate_gradient(input_qa=inputs, results=output, save_completed=save, 
                            completed_file=save_file, clean_up=clean_up, verbosity=verbosity,
                            topdown=topdown, topstart=topstart, eye=eye, img_thresh=img_thresh,
                            edge_thresh=edge_thresh, count_back=count_back, 
                            badscan_maxcount_back=badscan_maxcount_back)






if __name__ == '__main__':
    main()
